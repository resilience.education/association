# Association resilience.education

Describe at :
https://gitlab.com/resilience.education/association/-/blob/main/STATUTES.md

# Contributing
Describe at: 
https://gitlab.com/resilience.education/association/-/blob/main/CONTRIBUTING.md

# Authors and acknowledgment

## PROJECT application
Contact a BOARD member or COUNCIL member.

## MEMBER application
Contact a BOARD member or COUNCIL member.

## BOARD
* PRESIDENT : TBD
* SECRETARY(ies) : TBD
* TREASURER(s) : TBD

## COUNCIL
* MENTOR(s) : @nicolas_cantu

## SUPPORTED PROJECTS
* [4nk demo](https://gitlab.com/4nk1Main/signet-app-demo)
  * ARCHITECT(s) : @nicolas_cantu
  * MEMBER(s) : @yassine.yadine, @johanl, @674694@protonmail.ch
  * CANDIDATE(s) : 

# Usage

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:7c0211f96395743040f9ee6d9039eded?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:7c0211f96395743040f9ee6d9039eded?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:7c0211f96395743040f9ee6d9039eded?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/resilience.education/association.git
git branch -M main
git push -uf origin main
```

# License
Describe at: 
https://gitlab.com/resilience.education/association/-/blob/main/LICENSE

# Project status
Active
