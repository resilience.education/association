# Rôles

* BOARD : Le bureau de l'association composé des rôles :
  * PRESIDENT : Un(e) président(e) de l'association/
  * SECRETARY : Un(e) secrétaire et, s’il y a lieu, un(e) secrétaire(e) adjoint.
  * TREASURER : Un(e) trésorier(e), et, si besoin est, un(e) trésorier(e) adjoint.

* COUNCIL : L'ensemble des mentors de l'association composé des rôles :
  * MENTOR : Un(e) développeur(euse) contribuant significativement aux projets soutenus par l'association.

* PROJECT : Un projet composé des rôles :
  * ARCHITECT : Un(e) architecte dans au moins un de projets soutenus par l'association.
  * MEMBER : Un(e) contributeur(rice) des tickets des projets de l'association, un ARCHITECT est aussi un MEMBER.
  * CANDIDATE : Une personne physique ou morale qui souhaite contribuer à l'un des projets soutenus par l'association.

## CONSEIL D’ADMINISTRATION

L’association est dirigée par un conseil illimité de membres avec au moins 3 members, élus pour 3 années par l’assemblée générale. Les membres sont rééligibles.

Le conseil étant renouvelé chaque année par tiers, la première année, les MEMBERs sortants sont désignés par tirage au sort.

En cas de vacances, le conseil pourvoit provisoirement au remplacement de ses MEMBERs. Il est procédé à leur remplacement définitif par la plus prochaine assemblée générale. Les pouvoirs des MEMBERs ainsi élus prennent fin à l’expiration le mandat des MEMBERs remplacés.

Le conseil d’administration se réunit au moins trois fois par an, sur convocation du BOARD, ou à la demande du quart de ses MEMBERs.

Les décisions sont prises à la majorité des voix; en cas de partage, la voix du PRESIDENT est prépondérante.

En cas de démission d’un MEMBER du Conseil d’Administration, le PRESIDENT coopte un nouveau MEMBER pour la durée du mandat restant à couvrir.

## BUREAU

Les membres du BOARD sont aussi des MEMBERs.

Le PRESIDENT préside l’Assemblée Générale et à le pouvoir de représenter l’association dans tous les actes de vie civile.

Les membres sortants du BOARD sont rééligibles. En cas de départ ou de démission d’un membre du BOARD, le Conseil d’Administration élira un nouveau membre du bureau.

Ce bureau est qualifié pour régler les questions concernant la gestion courante de l’association dans l’intervalle de deux réunions du Conseil d’Administration.
Un membre peut avoir une ou plusieurs fonctions au sein du bureau.

## PROJECTs

Tout le monde peut demander le soutien de l'association pour un PROJECT. Le PROJECT candidat doit en faire la demande auprès d'un des membres du BOARD ou du COUNCIL.

La demande sera examinée par un MENTOR et le demandeur obtiendra un rôle d'ARCHITECT sur le PROJECT ajouté en quelques heures (jusqu’à 24).

Ensuite, le COUNCIL commencera à lui proposer des MEMBERs comme avec tous les autres PROJECTs.

# Outils

* TICKET MANAGEMENT : L'outil de gestion de tickets représentant :
  * TICKET : Une action estimée en temps et taggée en compétance à mener sur un projet

* SKILLS : Compétance référencée par l'association pour les contributions aux TICKETs des PROJETs :
  * TEACHER : Contributions aux TICKETs d'actions éducatives et de vulgarisation
  * CODER : Contributions aux TICKETs de la réalisation des projets
  * CONSULTANT : Contribution aux TICKETs de rapports et études sur la résilience
  * COMMUNITY MANAGER : Contributions aux TICKETs d’animation des communautés et aux évènements
  * JURIST : Contributions aux TICKETs de soutien juridique des projets open source

# Affectation des actions des missions dans les projets soutenus par l'association répârtis par tickets

## Si un CANDIDATE est totalement nouveau dans l’association

Le CANDIDATE doit postuler à l’un des PROJECT de démonstration.

La demande sera examinée par un MENTOR et le CANDIDATE obtiendra un rôle de MEMBER dans le projet en quelques heures (jusqu’à 24).

Ensuite, le COUNCIL commencera à lui proposer des missions comme avec tous les autres MEMBERs.

Le projet de démonstration par défaut est : https://gitlab.com/4nk1Main/signet-app-demo.

## Si le CANDIDATE connaît déjà le PROJECT qu’il souhaite le rejoindre

Le CANDIDATE demande à ARCHITECT dans le PROJECT de lui assigner un rôle de MEMBER dans le PROJECT et de recevoir des TICKETs s'ils ne sont pas tous fermés, il peut aussi en ouvrir de nouveaux et en rouvrir d’anciens.

# TICKETs

## Les premiers TICKETs

### Estimation des durées

Les premiers TICKETs nécessitent probablement plus de temps que le budget de 1 heure de réalisation et 10% de temps futur en dette dont elles disposent. 

### Commentaires et questions

Les MEMBERs ne doivent pas essayer de les apprendre, ni d’étudier ce qui a été fait; au lieu de cela, le nouveau MEMBER doit soumettre de nouveaux TICKETs, en demandant au PROJECT de documenter les choses et de résoudre toutes les préoccupations. 

Le MEMBER sera alors indemnisé pour ces TICKETs.

## Complétion des TICKETs

Les MEMBERS ne sont pas censés toujours compléter entièrement les TICKETs. Les TICKETs laissés incomplets sont dans la colonne “Current Sprint”.

## Echanges sur les TICKETs

L’association souhaite minimiser les chats, les réunions et les appels téléphoniques. L’association ne discute pas des actions techniques en dehors des TICKETs. Si les MEMBERs ont une question sur un TICKET, il envoi un message sur le TICKET.

# Remuneration

## Conseil d'administration et du BOARD

Toutes les fonctions, y compris celles du Conseil d'administration et du BOARD, sont gratuites et bénévoles. 

Seuls les frais occasionnés par l’accomplissement de leur mandat sont remboursés sur justificatifs. 

Le rapport financier présenté à l’assemblée générale ordinaire présente, par bénéficiaires, les remboursements de frais de mission, de déplacement ou de représentation.

## MEMBERs

Seul le temps passé par les MEMBERs sur les TICKETs seront indemnisés sur la base d’un taux journalier commun à tous les profils sur la base :

* TEACHER : 380 €*
* CODER : 280 €*
* CONSULTANT : 980 €*
* COMMUNITY MANAGER : 80 €*
* JURIST : 780 €*

Le montant annuel total de ces indemnités versées par personne ne pouvant pas excéder 37 000 € hors taxe et hors frais et ne pouvant pas excéder les montant légaux.

\* Maximum hors taxe et hors frais par jour, à la condition d’une marge positive supérieure à 50% pour l’association sur la mission; le cas échéant le temps passé ne sera pas indemnisé.

