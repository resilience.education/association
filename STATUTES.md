# STATUTS de l’association : resilience.education

## ARTICLE PREMIER – NOM

Il est fondé entre les membres aux présents statuts une association régie par la loi du 1er juillet 1901 et le décret du 16 août 1901, ayant pour titre : resilience.education.

## ARTICLE 2 – BUT OBJET

Resilience.education est une association du domaine de l’éduction qui a pour objet :
* Éduquer tous les publics à la résilience dans une ère post moderne et numérique dans une perspective de développement durable (responsabilité environnementale, économique et sociétale vis à vis des ressources limitées et indispensables à la vie et à la paix);
* Enseigner au travers pratique de contributions aux projets open source utiles à la résilience de chacun et des systèmes usuels;
* Dispenser des cours sous l'angle de la résilience appliquée aux ruptures majeures : la monnaie, l'énergie et le numérique à travers des contributions sur des projets structurant et d'envergure;
* Réaliser avec des professionnels et des enseignants pluridisciplinaires des missions ponctuelles d'études  de la résilience des personnes et des organisations;
* Créer les liens entre les personnes et les organisations et les projets et talents ambitieux de la résilience en France et à l'international;
* Aider au sponsoring en talents et financier ponctuellement par des tiers, des projets de résilience;
* Favoriser et fédérer l’échange des savoirs, des compétences  et des expériences autour de projets concrets;
* Rechercher et favoriser le développement de projets innovants et d’activités notamment dans le domaine de l’économie sociale et solidaire et d’actions humanitaires (bancarisation, accompagnement à la transition énergétique…);
* Défendre les droits des licences et contributions open source du domaine;
* Défendre et conseiller les utilisateurs des solutions open source du domaine;

Par les professionnels membres (éducation, monnaie, énergie, numérique, et...). Un numéro de formateur sera demandé en ce sens pour les formations.

## ARTICLE 3 – MOYENS MIS EN OEUVRE

L’association sera amener à mettre tous les moyens susceptibles de concourir à la réalisation de son objet éducatif, tels que :
* Participer ou contribuer à, créer et coordonner des actions éducatives (formations dans les écoles, universités, entreprises et collectivités, webinar, évènements, hackathons…) dans le domaine de la résilience, 
* Participer ou contribuer à, créer et coordonner des actions, des initiatives et des réalisations (contributions en compétences et expérimentation en transformant les “last adopters” en “early adopters”) tendant aux mêmes fins, en France, dans l’union européenne comme dans tout autre pays,
* Participer ou contribuer à, créer et coordonner des actions d’intégration et de vulgarisation au sein des projets majeur de la résilience à travers des apports en talents;
* Participer ou contribuer à, créer et coordonner des actions d’intégration et de vulgarisation au sein des projets majeur de la résilience à travers du sponsoring notamment financier par des tiers;
* Oeuvrer pour l’exercice de la résilience des personnes et des organisations, leur permettre un accès par eux même à la résilience, grâce à la connaissance;
* Promouvoir, créer, conseiller, aider, gérer directement ou indirectement tout dispositif permettant la résilience monétaire, énergétique et numérique, et intégrer dans cette démarche de nouveaux entrants tels que les PME/PMI, des collectivités, etc...
* S’inscrire et s’intégrer dans les réseaux actifs sur les territoires et en développer d’autres;
* Être force de propositions, études, rapports, et de lobbying pour les grandes décisions qui concernent la souveraineté des individus et des Sociétés;
* Mener des missions de défense des droits de projets open source du domaine;
* Mener des missions de défense des droits de utilisateurs de projets open source du domaine ;
* Oeuvrer pour une reconnaissance du groupement comme acteur incontournable de la résilience auprès des institutions publiques;
* Faire partie des cadres de négociation et de concertation auprès des instances décisionnaires. 

## ARTICLE 4 – SIÈGE SOCIAL

Le siège social est fixé au 4 square des Goélands, 76130 Mont Saint Aignan.
Il pourra être transféré par simple décision du conseil d’administration.

## ARTICLE 5 – DUREE

La durée de l’association est illimitée.

## ARTICLE 6 – COMPOSITION

L’association se compose de membres :
1. professionnels proposant des actions liées à l’activité de l’association et,
2. de personnes physiques qualifiées et intéressées par le développement d’actions de l’association et,
3. de membres qui codent dans le cadre de leurs activités et doivent avoir des bases de programmation électronique ou numérique.

Les membres participent aux décisions prises par l’assemblée générale à raison d’une voix délibérative par personne.

## ARTICLE 7 – ADMISSION / RETRAIT

Se référer à : https://gitlab.com/resilience.education/association/-/blob/main/CONTRIBUTING.md

## ARTICLE 8 – RESSOURCES

Les ressources de l’association comprennent :
* Le montant de la cotisation fixé chaque année par l’Assemblée Générale,
* les subventions pouvant lui être accordées par l’Europe, l’Etat, la Région, la Métropole, les Départements, les communes, les fondations ou tout autre organisme,
* les dons matériels et immatériels et les sponsoring de tiers,
* de toutes formes de ressources conformes à la législation en vigueur et contribuant au développement du but de l’association.

L’association pourra exercer des activités :
* de formation auprès de jeunes et de professionnels,
* d’études et de rapports sur la résilience auprès des entreprises et des institutions,
* de réalisation de projets de résilience auprès d’entreprises, et institutions,
* de création et d’animation de communautés pluridisciplinaires contribuant à la démocratisation des moyens de résilience incluant des évènements possiblement payant,
* de campagnes d’appel aux dons pour le financement de projets open source,
* de campagnes d’appel aux talents pour la contribution à la réalisation de projets open source.

Certains TICKETs pourront être sous-traitées si aucun des MEMBERs des projets soutenus par l'association ne souhaite contribuer. A chaque nouveau TICKET, l’ensemble des MEMBERs sont appelés à contribuer. Les MEMBERs peuvent accepter ou refuser librement de participer.

## ARTICLE 9 – ASSEMBLEE GENERALE ORDINAIRE

L’assemblée générale ordinaire comprend tous les MEMBERs de l’association à quelque titre qu’ils soient.

Elle se réunit dans le premier semestre de chaque année.

La présence physique du quart au moins des MEMBERs est nécessaire pour la validité des délibérations. En cas d’absence de quorum, une nouvelle assemblée générale sera convoquée dans un délai de 30 jours.

Quinze jours au moins avant la date fixée, les MEMBERs de l’association sont convoqués par les soins du SECRETARY. L’ordre du jour figure sur les convocations.

Le PRESIDENT, assisté des membres du conseil, préside l’assemblée et expose la situation morale ou l’activité de l’association.

Le TREASURER rend compte de sa gestion et soumet les comptes annuels (bilan, compte de résultat et annexe) à l’approbation de l’assemblée. L’assemblée générale fixe le montant des cotisations annuelles et du droit d’entrée à verser par les différentes catégories de MEMBERs.
Ne peuvent être abordés que les points inscrits à l’ordre du jour sauf proposition reçue au moins 5 jours à l’avance, au siège social de l’association.

Les décisions sont prises à la majorité des voix des MEMBERs présents ou représentés. Il est procédé, après épuisement de l’ordre du jour, au renouvellement des membres sortants du conseil.

Le mode de scrutin pour l’élection des membres du conseil et toutes délibérations est déterminé par le PRESIDENT de séance.

Les décisions des assemblées générales s’imposent à tous les MEMBERs, y compris absents ou représentés.

## ARTICLE 10 – ASSEMBLEE GENERALE EXTRAORDINAIRE

Si besoin est, ou sur la demande de la moitié plus un des MEMBERs inscrits, le BOARD peut convoquer une assemblée générale extraordinaire, suivant les modalités prévues par l’article 9.

## ARTICLE 11 – CONSEIL D’ADMINISTRATION

Se référer à : https://gitlab.com/resilience.education/association/-/blob/main/CONTRIBUTING.md

## ARTICLE 12 – LE BUREAU

Se référer à : https://gitlab.com/resilience.education/association/-/blob/main/CONTRIBUTING.md

## ARTICLE 13 – INDEMNITÉS

Se référer à : https://gitlab.com/resilience.education/association/-/blob/main/CONTRIBUTING.md

## ARTICLE 14 – RÈGLEMENT INTÉRIEUR

Un règlement intérieur peut être établi par le conseil d’administration, qui le fait alors approuver par l’assemblée générale.

Ce règlement éventuel est destiné à fixer les divers points non prévus par les présents statuts, notamment ceux qui ont trait à l’administration interne de l’association.

Se référer à : https://gitlab.com/resilience.education/association/-/blob/main/CONTRIBUTING.md

## ARTICLE 15 – DISSOLUTION

En cas de dissolution prononcée selon les modalités prévues à l’article 10, un ou plusieurs liquidateurs sont nommés, et l’actif, s’il y a lieu, est dévolu conformément aux décisions de l’assemblée générale extraordinaire qui statue sur la dissolution.


« Fait à Mont Saint Aignan, le 8 septembre 2021 »

